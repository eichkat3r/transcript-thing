import json
import os

import click
import deepspeech
import numpy as np
import shutil
import wget

from .backend import TranscriptionBackend
from ..paths import MODELS_PATH


class DeepspeechBackend(TranscriptionBackend):
    def __init__(self):
        super().__init__()
        self.model_urls = {
            'en': 'https://github.com/mozilla/DeepSpeech/releases/download/v0.9.3/models_0.9.tar.gz'
        }
        self.model_dirs = {
            'en': 'models'
        }
        self.model = None

    def get_models_path(self):
        return os.path.join(MODELS_PATH, 'deepspeech')

    def get_language_model_path(self, language):
        return os.path.join(self.get_models_path(), self.model_dirs[language])

    def get_model_url(self, language, default=None):
        return self.model_urls.get(language, default)

    def download_model(self, language):
        model_url = self.get_model_url(language, None)
        if not model_url:
            click.secho(f'No model available.', fg='yellow')
            return
        os.makedirs(self.get_models_path(), exist_ok=True)
        tar_path = os.path.join(self.get_models_path(), 'models.tar.gz')
        wget.download(model_url, out=tar_path)
        print()
        click.secho(f'Download finished. Extracting tgz...', fg='blue')
        shutil.unpack_archive(tar_path, self.get_language_model_path(language))

    def start(self, language, samplerate=44100):
        model_file_path = os.path.join(
            self.get_language_model_path(language),
            'deepspeech-0.9.3-models.pbmm'
        )
        self.model = deepspeech.Model(model_file_path)
        scorer_path = model_file_path[:-len('.pbmm')] + '.scorer'
        self.model.enableExternalScorer(scorer_path)

    def process(self, buffer):
        data = np.frombuffer(buffer, dtype=np.int16)
        self.result = self.model.stt(data)
        self.partial = self.result

    def end(self):
        pass
