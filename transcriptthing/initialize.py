#!/usr/bin/env python3
import json
import os

import click
import wget
import zipfile

from .backend import voskbackend
from .paths import CONFIG_PATH, MODELS_PATH
from .utils import get_language_code, get_backend


def download_model(backend, add_english=False):
    lang = get_language_code()

    # download system default model, and English model as a fallback
    languages = [lang]
    if add_english and 'en' not in languages:
        languages.append('en')
    # download all required models
    for lang in languages:
        click.secho(f'Downloading model for language code {lang}', fg='blue')
        backend.download_model(lang)


def initialize():
    backend = get_backend()
    download_model(backend)


if __name__ == '__main__':
    initialize()
