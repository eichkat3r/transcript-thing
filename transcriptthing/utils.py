import json
import locale
import os
import shutil

import click

from .backend import BACKENDS
from .paths import CONFIG_PATH, RESOURCES_PATH


def load_config():
    if not os.path.exists(CONFIG_PATH):
        example_conf_path = os.path.join(RESOURCES_PATH, 'config.json.example')
        shutil.copyfile(example_conf_path, CONFIG_PATH)
    with open(CONFIG_PATH, 'r') as f:
        config = json.load(f)
    return config


def get_language_code():
    lang, encoding = locale.getdefaultlocale()
    lang = lang[:2]
    config = load_config()
    return config.get('language', lang)


def get_backend(default='vosk'):
    config = load_config()
    backend_name = config.get('backend', default)
    backend = BACKENDS.get(backend_name, BACKENDS['vosk'])()
    return backend

