import os
import sys
from random import seed, choice
from string import ascii_letters
from threading import Thread

import dbus


def textdump():
    session_bus = dbus.SessionBus()
    bus_object = session_bus.get_object(
        'de.eichkat3r.TranscriptThing',
        '/ChannelObject'
    )
    channel_iface = dbus.Interface(
        bus_object,
        dbus_interface='de.eichkat3r.TranscriptThing.ChannelInterface'
    )

    previous_text = ''
    previous_res = ''
    while True:
        text = channel_iface.GetPartial()
        if text != previous_text and text != '':
            print('part:', text)
            previous_text = text
        res = channel_iface.GetResult()
        if res != '' and res != previous_res:
            print('result:', res)
            previous_res = res

