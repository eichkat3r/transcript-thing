import os


USER_HOME_PATH = os.path.expanduser('~')
SOURCE_PATH = os.path.abspath(os.path.dirname(__file__))
ROOT_PATH = os.path.join(SOURCE_PATH, '..')
CONFIG_PATH = os.path.join(USER_HOME_PATH, '.transcriptthing', 'config.json')
MODELS_PATH = os.path.join(ROOT_PATH, 'models')
RESOURCES_PATH = os.path.join(SOURCE_PATH, 'resources')

os.makedirs(os.path.abspath(os.path.dirname(CONFIG_PATH)), exist_ok=True)
