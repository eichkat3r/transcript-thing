import ctypes
import os

import numpy as np
import soundfile as sf

from .translation import ArgosTranslator
from .utils import get_language_code, get_backend


def filedump(filename):
    # TODO find out if this is a reasonable block size
    block_size = 100000

    language = get_language_code()
    translator = ArgosTranslator(language)
    translator.select_output_language('en')
    translator.select_model()

    # load input file
    blocks = sf.blocks(filename, blocksize=block_size, dtype='int16')
    samplerate = 44100
    
    # select backend from config and initialize
    backend = get_backend()
    backend.start(language, samplerate)

    for i, block in enumerate(blocks):
        backend.process(block.tobytes())
        partial = translator.translate(backend.partial)
        result = translator.translate(backend.result)
        print(result)
    backend.end()
